const test = require('tape')
const leveldown = require('../leveldown')
const testCommon = require('abstract-leveldown/testCommon')

const testOpenForReadOnly = function () {
  test('test database open read only', function (t) {
    var db = leveldown(testCommon.location())
    db.open({ readOnly: true }, function (err) {
      t.error(err)
      db.open({ readOnly: true }, function (err) {
        t.error(err)
        db.close(function () {
          t.end()
        })
      })
    })
  })
}

const testOpenThrows = function () {
  test('test database-open-multiple, throws', function (t) {
    var db = leveldown(testCommon.location())
    db.open(function (err) {
      t.error(err)
      t.throws(
        db.open.bind(db, {})
        , { name: 'Error', message: ' IO error: Failed to create lock file' }
        , 'database-open-multiple throws'
      )
      db.close(function () {
        t.end()
      })
    })
  })
}

testOpenForReadOnly()
testOpenThrows()
