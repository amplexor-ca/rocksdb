# rocksdb

> Fork of the `rocksdb` project, version **3.0.3**: [https://github.com/Level/rocksdb](https://github.com/Level/rocksdb)

[![npm](https://img.shields.io/npm/v/rocksdb.svg?label=&logo=npm)](https://www.npmjs.com/package/rocksdb)
[![Node version](https://img.shields.io/node/v/rocksdb.svg)](https://www.npmjs.com/package/rocksdb)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## rocksdb original README

> For more detailed explanation on how this module works, refer to the documentation of the original `rocksdb` project that can be found here: [README.md](./README_RocksDB.md).

## API

This fork of `rocksdb` implements the same API as the orignal `rocksdb` project.

## Additions to the original rocksdb module

This project adds the ability to start the database for read only. This allows multiple threads to read from the same database simultaneously.

To do so, an `boolean:readOnly` attribute has been added to the configuration that is passed to the `open` function.

When the attribute `readOnly` value is `true`, the database is opened for read only. By default, the value of this attribute is `false`.

```js
const db = leveldown('.\mydb')
// Open database for read only
db.open({ readOnly: true }, (err) => { ... });
```

## Important information

- **When opening the database for read only, all DB interfaces that modify data, like put/delete, will return error.**
- If the db is opened in read only mode, then no compactions will happen.
- Not supported in `ROCKSDB_LITE`, in which case the function will return a status value of `NotSupported`.